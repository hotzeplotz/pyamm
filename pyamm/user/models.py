# -*- coding: utf-8 -*-
"""User model with LDAP"""
#pylint: disable=unused-import
import hashlib
import os
from base64 import encodestring as encode
from base64 import decodestring as decode
from flask import g, current_app

from flask_login import UserMixin
from ldap3 import MODIFY_ADD, MODIFY_REPLACE, MODIFY_DELETE
from ldap3.core.exceptions import LDAPConnectionIsReadOnlyError

from pyamm.extensions import ldap
#from pyamm.utils import get_current_time
from pyamm.constants import USER_ROLE, ADMIN, USER_STATUS, ACTIVE

USER = dict()

class User(UserMixin):
    """User model.
    This model follows the LDAP object plugin system."""

    def __init__(self, dn, username, data):
        """Create a new user attributes:
         - dn: distinguished name
         - username: the username corresponds with its pyamm email
         - data: all others available ldap attributes"""
        self.dn = dn
        self.username = username
        self.data = data

    def __repr__(self):
        return self.dn

    @property
    def email(self):
        """Add readonly attribute"""
        return self.username

    def get_id(self):
        return self.dn

    # Class methods
    @classmethod
    def authenticate(cls, username, password):
        """Try to authenticate against LDAP"""
        result = ldap.authenticate(username, password)
        return result

    @staticmethod
    def user_is_loaded(distinguishedname):
        """Check if user has already loaded into db"""
        if distinguishedname in USER:
            return USER[distinguishedname]
        else:
            return None

    @classmethod
    def update_user(cls, distinguishedname):
        """Update the account password"""
        if cls.user_is_loaded(distinguishedname):
            del USER[distinguishedname]
            #new_user_data = ldap.get_user_info(distinguishedname)
            #if new_user_data:
            #    current_app.logger.info(USER[distinguishedname])
            #    USER[distinguishedname] = new_user_data
            #    current_app.logger.info(USER[distinguishedname])

